#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import csv
import sys
import colorama
from colorama import Fore

# Réinitialisation de la couleur après chaque 'print'
colorama.init(autoreset=True)

# Test pour vérifier si la saisie d'un nom de fichier est correcte
if len(sys.argv) < 2:
    print(Fore.RED + '----------------------------------------------------')
    print(Fore.RED + 'Veuillez préciser le nom du fichier CSV à traiter...')
    print(Fore.RED + '----------------------------------------------------')
    exit(0)

# Récupération du nom du fichier et de son extension
document = os.path.abspath(sys.argv[1])
(pathname, fullname) = os.path.split(document)
(filename, extension) = os.path.splitext(fullname)

# Test pour vérifier si le fichier et son extension sont corrects
if os.path.exists(document) != True or extension != '.csv' or '_convert' in fullname:
    print(Fore.RED + '------------------------------------------')
    print(Fore.RED + 'Veuillez indiquer un fichier CSV valide...')
    print(Fore.RED + '------------------------------------------')
    exit(0)

# Ouverture du fichier CSV
infile = open(document, 'r')

# Affichage du processus en cours
print('\nTraitement du fichier ' + Fore.YELLOW + fullname + Fore.RESET + ' en cours...')

# Création de la liste des Objets
collect = []
for row in csv.reader(infile):
    for datum in row[1].split(' '):
        collect.append(datum)
list_data = sorted(list(set(collect)))

# Affichage du nombre d'Objets enregistrés
print('├─> ' + Fore.YELLOW + str(len(list_data)) + Fore.RESET + ' Objets enregistrés')

# Création du tableau Objets / Références
output = open(filename + '_convert.csv', 'w')
for record in list_data:
    infile.seek(0)
    output.write(record + ',')
    state = 0
    for row in csv.reader(infile):
        if record in row[1].split(' '):
            if state == 1:
                output.write(' ')
            output.write(row[0])
            state = 1
    output.write('\n')
output.close()

# Fermeture du fichier CSV
infile.close()

# Affichage du résultat
print('├─> ' + Fore.GREEN + filename + '_convert.csv' + Fore.RESET + ' créé avec succès !')

# Création d'une liste contenant exclusivement des numéros d'Objets
list_nb = []
for item in list_data:
    if item.isdigit():
        list_nb.append(int(item))

# Création d'une liste complète (estimation)
checklist = list(range(min(list_nb), max(list_nb) + 1))

# Création de la liste des numéros manquants
missing = []
for num in checklist:
    if num not in list_nb:
        missing.append(num)

# Affichage du nombre d'Objets manquants
print('└─> ' + Fore.RED + str(len(missing)) + Fore.RESET + ' Objets sont manquants sur un total de ' + Fore.MAGENTA + str(len(checklist)) + ' (estimation)')

# Affichage des numéros d'Objets manquants
print('Liste des Objets manquants : ' + Fore.RED + str(missing))
