# tabconvert.py

Ce script permet de convertir un tableau CSV en classant les informations de la première colonne en fonction des données de la deuxième colonne.

## Prérequis

  * python3-colorama

## Utilisation

Le lancement s'effectue dans un terminal :

```
$ python3 tabconvert.py /chemin/vers/mon/fichier.csv
```

## Fonctionnement

Le fichier CSV à traiter doit contenir 2 colonnes, sur ce modèle :

| Références | Objets              |
|------------|---------------------|
| 401        | 2001 2002 2006      |
| 402        | 2003 2004 2005      |
| 403        | 2002 2004           |
| 404        | 2007 2008 2009 2010 |
| 405        | 2008 2002 2001 2006 |
| 406        | 2006 2010           |

Après traitement du tableau ci-dessus, voici le résultat :

| Objets | Références  |
|--------|-------------|
| 2001   | 401 405     |
| 2002   | 401 403 405 |
| 2003   | 402         |
| 2004   | 402 403     |
| 2005   | 402         |
| 2006   | 401 405 406 |
| 2007   | 404         |
| 2008   | 404 405     |
| 2009   | 404         |
| 2010   | 404 406     |

## Licence

Le programme ci-joint est publié sous licence GPLv3.  
[www.gnu.org/licenses/gpl-3.0.fr.html](https://www.gnu.org/licenses/gpl-3.0.fr.html)
